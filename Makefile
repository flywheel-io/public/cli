BASE                = $(GOPATH)/src/$(PACKAGE)
SRC                 = $(shell find . -type f -name '*.go' -not -path "./vendor/*" -not -path "./builds/*" -not -path "./python/pkgdata/*")
DIST                = $(CURDIR)/dist
GLIDE               = glide
GO                  = go
GOFMT               = $(GO)fmt
GOPATH              = $(CURDIR)/.gopath
GOX                 = gox
OSARCH             := "darwin/amd64 linux/amd64 windows/amd64"
PACKAGE             = flywheel.io/fw
PLATFORMS          := darwin linux windows
PYTHON_BUILD        = $(PYTHON_DIR)/build
PYTHON_DIR          = $(CURDIR)/python
PYTHON_DIST         = $(PYTHON_DIR)/dist
PYTHON_PKGDATA      = $(PYTHON_DIR)/pkgdata
PYTHON_VENV         = $(PYTHON_DIR)/.venv
CLI_VERSION        ?= dev
BUILD_HASH         ?= dev
BUILD_DATE         ?= dev
COMMON_LDFLAGS      = -X "main.Version=$(CLI_VERSION)" -X "main.BuildHash=$(BUILD_HASH)" -X "main.BuildDate=$(BUILD_DATE)"

.PHONY: all
all: fmt vendor | $(PYTHON_PKGDATA) $(PYTHON_VENV) $(BASE)
	cd $(BASE) && GOPATH=${GOPATH} $(GOX) -ldflags '$(COMMON_LDFLAGS)' -osarch=$(OSARCH) -output "bin/{{.OS}}_{{.Arch}}/fw"
	@# cd $(BASE) && $(GO) build -o bin/$(PACKAGE) fw.go

.PHONY: useMusl
useMusl: all
	CC=`which musl-gcc` GOPATH=${GOPATH} go build -v -ldflags '$(COMMON_LDFLAGS) -linkmode external -extldflags "-static"' -o bin/linux_amd64/fw fw.go

$(PYTHON_VENV): | $(BASE)
	cd $(BASE) && python3.6 -m venv $(PYTHON_VENV)
	( \
		. $(PYTHON_VENV)/bin/activate; \
		pip install -r $(PYTHON_DIR)/build-requirements.txt; \
		PYTHON_CLI_VERSION=$$PYTHON_CLI_VERSION python $(PYTHON_DIR)/compile.py; \
	)

$(BASE):
	@mkdir -p $(dir $@)
	@ln -sf $(CURDIR) $@

.PHONY: fmt
fmt: ## Run gofmt on all source files
	$(info running gofmt…)
	@$(GOFMT) -l -w $(SRC)

glide.lock: glide.yaml | $(PYTHON_PKGDATA) $(BASE)
	cd $(BASE) && $(GLIDE) update
	@touch $@

$(PYTHON_PKGDATA): | $(PYTHON_VENV) ## Insert packaged data
	$(info running go-bindata...)
	ls $(PYTHON_DIST)
	mkdir -p $(PYTHON_PKGDATA)
	ret=0 && for platform in $(PLATFORMS); do \
	srcdir="$(PYTHON_DIST)/$${platform}-x86_64" ; \
	dstfile="$(PYTHON_PKGDATA)/pkgdata_$${platform}.go" ; \
	go-bindata -nocompress -pkg="pkgdata" -prefix="$$srcdir" -o="$$dstfile" "$$srcdir" || ret="$$?" ; \
	done ; exit "$$ret"

vendor: glide.lock | $(BASE) ; $(info $(M) retrieving dependencies...)
	$Q cd $(BASE) && $(GLIDE) --quiet install
	@ln -nsf . vendor/src
	@touch $@

.PHONY: clean
clean: ## Cleanup everything
	$(info cleaning...)
	rm -rf $(PYTHON_BUILD) $(PYTHON_DIST) $(PYTHON_PKGDATA) $(PYTHON_VENV)
	rm -rf bin/ vendor/ dist/
	rm -rf $(GOPATH)

.PHONY: fmtCheck
fmtCheck:
	@if [ $(shell $(GOFMT) -l $(SRC)) ]; then echo "Format check failed"; exit 1; fi

archive: useMusl
	mkdir -p $(DIST)
	cd $(BASE)/bin/ && for arch in "darwin_amd64" "windows_amd64" "linux_amd64"; do \
	zip -r "$(DIST)/fw-$${arch}.zip" "$${arch}" ; \
	done
