
# Flywheel Command-Line Tool
![pipeline status](https://gitlab.com/flywheel-io/public/cli/badges/master/pipeline.svg)

## Building

### Requirements

In order to build the binaries, the following things are needed
- python 3.6 with virtualenv and pip
- Environment Variable `PYTHON_CLI_VERSION` set to the desired python-cli version
- golang 1.12.5
- `go get github.com/mitchellh/gox`
- `go get github.com/jteeuwen/go-bindata/...`

### Building Executable

```bash
make
```

The binary will be compiled to `bin/{OS}_{ARCH}/fw`.

#### Fresh build

```bash
make clean
make
```

#### Build using local python source

Set the `PYTHON_CLI_SOURCE` environment variable to point to `python-cli`
repository.

```bash
PYTHON_CLI_SOURCE=~/workspace/flywheel/python-cli make
```

## Interacting with a Flywheel instance

First, you need to generate an API key via your profile page.
Login using the CLI with the URL of the site and your API key:

```
$ fw login dev.flywheel.io:Xz6SLBbDFu0Zne6uA1
Logged in as Nathaniel Kofalt!
```

These credentials will be stored in `~/.config/flywheel`.
You can now explore and download files from the storage hierarchy:

```
$ fw ls
scitran Scientific Transparency

$ fw ls scitran
Testdata
Neuroscience
Psychology

$ fw ls scitran/Neuroscience
patient_2
patient_1
control_1
control_2
patient_343

$ fw ls scitran/Neuroscience/patient_1
8403_6_1_fmri
8403_4_1_t1
8403_1_1_localizer

$ fw ls scitran/Neuroscience/patient_1/8403_1_1_localizer
8403_1_1_localizer.dicom.zip

$ fw download scitran/Neuroscience/patient_1/8403_1_1_localizer/8403_1_1_localizer.dicom.zip
```

## Creating a release

When creating a new CLI release, tag [`python-cli`](https://gitlab.com/flywheel-io/public/python-cli)
first then push the same tag on this repository.
Non-tag builds use the latest `python-cli` from PyPI.
